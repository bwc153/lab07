﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    public enum CharacterState
    {
        Idle = 0,
        WalkingForwards = 1,
        WalkingBackwards = 2,
        RunningFowards = 3,
        Jumping = 4,
        RunningBackwards = 5,
    };
    public Animator controller;
    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;

    }
    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverState;

    PlayerState predictedState;
    CharacterState characterAnimationState;
    Queue<KeyCode> pendingMoves;
    [Tooltip("Walk speed")]
    public float walkSpeed = 0.1f;
    [Tooltip("Jump speed")]
    public float jumpSpeed = 0.1f;
    [Tooltip("Multiplier for sprint speed")]
    public float sprintSpeed = 3f;


    [Tooltip("Speed of strafing")]
    public float strafeSpeed = 0.1f;
    [Tooltip("Speed of turning")]
    public float turnSpeed = 1.5f;


    // Use this for initialization
    void Start()
    {
        InitState();
        predictedState = serverState;
        if (isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }
    [Server]
    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }
    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer)
        {
            //Debug.Log("Pending Moves: " + pendingMoves.Count);
            KeyCode[] moveKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space, KeyCode.LeftShift, KeyCode.Alpha0 };
            bool somethingPressed = false;

            foreach (KeyCode moveKey in moveKeys)
            {
                if (!Input.GetKey(moveKey))
                {
                    continue;
                }

                //Debug.Log(moveKey);
                somethingPressed = true;
                pendingMoves.Enqueue(moveKey);
                //Debug.Log("Pending Moves: " + pendingMoves.Count);
                UpdatePredictedState();
                CmdMoveOnServer(moveKey);

                if (!somethingPressed)
                {
                    pendingMoves.Enqueue(KeyCode.None);
                    UpdatePredictedState();
                    CmdMoveOnServer(KeyCode.None);
                }
            }
            SyncState();

        }
    }

    void SyncState()
    {
        PlayerState statetoRender = isLocalPlayer ? predictedState : serverState;

        transform.position = new Vector3(statetoRender.posX, statetoRender.posY, statetoRender.posZ);
        transform.rotation = Quaternion.Euler(statetoRender.rotX, statetoRender.rotY, statetoRender.rotZ);
        controller.SetInteger("CharacterState", (int)statetoRender.animationState);

    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = -strafeSpeed;
                break;
            case KeyCode.W:
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    deltaZ = walkSpeed * sprintSpeed;
                    break;
                }
                else
                {
                    deltaZ = walkSpeed; break;
                }
            case KeyCode.E:
                deltaX = strafeSpeed;
                break;
            case KeyCode.S:
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    deltaZ = -walkSpeed * sprintSpeed;
                    break;
                }
                else
                {
                    deltaZ = -walkSpeed; break;
                }
            case KeyCode.A:
                deltaRotationY = -turnSpeed;
                break;
            case KeyCode.D:
                deltaRotationY = turnSpeed;
                break;
            case KeyCode.Space:
                deltaY = jumpSpeed;
                break;

        }

        //Vector3 pos = CalculateDistance(this.transform.rotation.y, deltaY, deltaZ, deltaX);

        return new PlayerState

        {


            movementNumber = 1 + previous.movementNumber,
            posX = deltaZ + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posY,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
        };
    }

    //  DOES NOT WORK - UNSURE OF OTHER METHODS TO TRY.
    //Calculates, in new Vector 3 position, movement for a rotated player.
    //Vector3 CalculateDistance(float radianA, float inputY, float inputZ, float inputX)
    //{
    //    float angleA = (Mathf.PI / 180) * radianA;   //Converting default Unity value to radians for Trigonometry
    //    float angleB = 90f;    
    //    float angleC = 180 - angleA - angleB;
    //    float line1;   //Hypotenuse
    //    float line2;
    //    float line3;

    //    if (inputX == 0)
    //    {
    //        line1 = inputZ;   //Hypotenuse
    //        line2 = (inputZ * Mathf.Sin(angleC)) / Mathf.Sin(angleB);
    //        line3 = (inputZ * Mathf.Sin(angleA)) / Mathf.Sin(angleB);
    //    }
    //    else if (inputZ == 0)
    //    {
    //        line1 = inputX;   //Hypotenuse
    //        line2 = (inputX * Mathf.Sin(angleC) / Mathf.Sin(angleB));
    //        line3 = (inputX * Mathf.Sin(angleA) / Mathf.Sin(angleB));
    //    }
    //    else
    //    {
    //        line2 = inputX;
    //        line3 = inputZ;
    //    }
    //     Debug.Log("L2: " + line2 + " L3: " + line3);
    //    return new Vector3(this.transform.position.x + line2, this.transform.position.y + inputY, this.transform.position.z + line3);
    //}

    void OnServerStateChanged(PlayerState newState)
    {
        serverState = newState;
        if (pendingMoves != null)
        {
            while (pendingMoves.Count > (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }
            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictedState = serverState;
        foreach (KeyCode moveKey in pendingMoves)
        {
            predictedState = Move(predictedState, moveKey);
        }
    }



    CharacterState CalcAnimation(float dX, float dY, float dZ, float dRY)
    {
        //Debug.Log("Delta X: " + dX + " " + "Delta Y: " + dY + " " + "Delta Z: " + dZ);
        //Debug.Log(characterAnimationState);
        if (dX == 0 && dY == 0 && dZ == 0)
            return CharacterState.Idle;

        if (dX != 0 || dZ != 0)
        {
            if (dZ == walkSpeed || dX == walkSpeed)
                return CharacterState.WalkingForwards;

            else if (dZ >= walkSpeed)
                return CharacterState.RunningFowards;

            else if (dZ == -walkSpeed || dX == -walkSpeed)
                return CharacterState.WalkingBackwards;

            else if (dZ <= -walkSpeed)
                return CharacterState.RunningBackwards;

            else
                return CharacterState.Idle;
        }
        return CharacterState.Idle;
    }
}